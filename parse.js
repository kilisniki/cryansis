let fs = require('fs');
let MongoClient = require('mongodb').MongoClient;
const connectionString = 'mongodb://localhost:27017';


const structblock = {
	_id: 0,
	number: 0,
	hash: '',
	date: 0,
	countTx: 0,
	countTxIn: 0,
	countTxOut: 0,

	//sumTx: 0,							//the same as sumTxOut
	sumTxIn: 0,   					
	sumTxOut: 0,		
	sumComsn: 0,

	averTxIn: 0,						// sumTxIn / count transaction !
	averTxOut: 0,						// sumTxOut / count transaction
	averAllIn: 0,						// sumTxIn / count transaction input 
	averAllOut: 0,						// sumTxOut / count transaction output
	averComsn: 0,

	medianTxIn: 0,
	medianTxOut: 0,
	medianAllIn: 0,
	medianAllOut: 0,
	//medianTx: 0,						//the same as medianTxOut
	medianComsn: 0,

	maxTx: 0,
	minTx: 0,
	maxAllIn: 0,
	minAllIn: 0,
	maxAllOut: 0,
	minAllOut: 0,
	maxComsn: 0,
	minComsn: 0,

	inputs: [],
	outputs: [],
	cmsn: [],
	sumin: [],
	sumout: []						
}




//var tx = '';
//var block = Object.create(structblock);
//var block = {};
var block = structblock;

function start(){
	fs.readFile('bitcoin.txt', 'utf8', function(err, contents) {
    	tx = JSON.parse(contents);
    	work();
	});
}


var sumTx = 0;
var sumTxInp = 0;
var countTx = 0;


async function parseTX(){	
	
	
	tx.tx.forEach(function(tranx, i, arrtx){
		if(i==0)											//pass first tx because it new mine monets
			return;

		let inparr = [];
		let outarr = [];

		let commiss = 0;
		let sumin = 0;
		let sumout = 0;


		tranx.inputs.forEach(function(txinp, j, arrin){
			inparr.push(txinp.prev_out.value);
			block.sumTxIn += txinp.prev_out.value;/* need refactoring*/
			block.countTxIn++;/* need refactoring*/
			sumin += txinp.prev_out.value;

		});
		tranx.out.forEach(function(txout, j ,arrout){
			outarr.push(txout.value);
			block.sumTxOut += txout.value;/* need refactoring*/
			block.countTxOut++;/* need refactoring*/
			sumout += txout.value;
		});

		block.inputs.push(inparr);
		block.outputs.push(outarr);

		block.cmsn.push(sumin - sumout);
		block.sumin.push(sumin);
		block.sumout.push(sumout);




	})
}


//comparator for sort
function compareNumeric(a, b) {
  if (a > b) return 1;
  if (a < b) return -1;
}

//integer division
function div(val, by){
    return (val - val % by) / by;
}


/*NEED OPTIMIZATION O(n) 
	(Tony Hoare or Blum-Floyd-Pratt-Rivest-Tarjan) */
async function sortmedianScore(arr){
	let median,
		half,
		mas = [];

	arr.forEach(function(e,i,array){
		mas.push(e);
	});

	mas.sort(compareNumeric);

	half = div(mas.length, 2);

	if(mas.length % 2)
		median = mas[half];
	else
		median = (mas[half] + mas[half-1]) / 2.0;

	return median;

}

//score sorted array's median
async function medianScore(arr){
	let median,
		half;

	half = div(arr.length, 2);

	if(arr.length % 2)
		median = arr[half];
	else
		median = (arr[half] + arr[half-1]) / 2.0;

	return median;
}

/*calculation of different characteristics 
	of a two-dimensional array */
async function mScore(TwDmArr){
	let allinputs = [],
		suminputs = [],
		result = {};
		

	TwDmArr.forEach(function(elements, i, arr){
		suminputs[i] = 0;
		elements.forEach(function(elem, j, arrt){
			allinputs.push(elem);
			suminputs[i] += elem;
		});
	});

	allinputs.sort(compareNumeric);
	suminputs.sort(compareNumeric);

	//Score Median of allinputs
	result.medianAll = await medianScore(allinputs);

	//Score Median of suminputs
	result.mediantx = await medianScore(suminputs);
	
	result.maxAll = allinputs[allinputs.length-1];
	result.minAll = allinputs[0];

	result.maxSum = suminputs[suminputs.length-1];
	result.minSum = suminputs[0];

	return result;
}

async function inputBlock() {
	block.hash = tx.hash;
	block.height = tx.height;

	block._id = tx.height+tx.block_index; //?!

	block.countTx = tx.n_tx;
	block.sumTxIn = block.sumTxIn; // / 100000000;
	block.sumTxOut = block.sumTxOut; // / 100000000;
	block.sumComsn = block.sumTxIn - block.sumTxOut;


	//Score Average values
	block.averTxIn = block.sumTxIn / block.countTx;
	block.averTxOut = block.sumTxOut / block.countTx;
	block.averAllIn = block.sumTxIn / block.countTxIn;
	block.averAllOut = block.sumTxOut / block.countTxOut;
	block.averComsn = block.sumComsn / block.countTx;

	//вычисляем медианы, максимумы, минимумы
	var mIn = await mScore(block.inputs);
	var mOut = await mScore(block.outputs);


	block.medianTxIn = mIn.mediantx;
	block.medianAllIn = mIn.medianAll;
	block.maxAllIn = mIn.maxAll;
	block.minAllIn = mIn.minAll;
	
	block.medianTxOut = mOut.mediantx;	
	block.medianAllOut = mOut.medianAll;
	block.maxAllOut = mOut.maxAll;
	block.minAllOut = mOut.minAll;

	//Я не знаю правильно ли это
	block.minTx = mOut.minSum;
	block.maxTx = mOut.maxSum;

	block.cmsn.sort(compareNumeric);
	block.medianComsn = await medianScore(block.cmsn);
	block.minComsn = block.cmsn[0];
	block.maxComsn = block.cmsn[block.cmsn.length-1];



	block.date = +new Date;
	//...

}

async function saveblock(){
	let client = await MongoClient.connect(connectionString,
            { useNewUrlParser: true });
	let db = client.db('CryAnsis');
        try {
           const res = await db.collection("ParseBlocs").insertOne(block);

           console.log(`res => ${JSON.stringify(res)}`);
        }
        finally {
            client.close();
        }

}

async function work(){
	await parseTX();
	await inputBlock();
	await saveblock();

	//console.log(block);
}


module.exports = {
    startParse: start
};

console.log(+new Date());
